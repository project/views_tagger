Views Tagger
================================================================================

Summary
--------------------------------------------------------------------------------

The Views Tagger module adds the taxonomy form elements from the node editing
form to a view. This makes it easy to assign taxonomy terms to many nodes at
once while seeing a preview of the nodes.


Requirements
--------------------------------------------------------------------------------

The View Tagger module requires the Taxonomy (core) and Views modules.


Installation
--------------------------------------------------------------------------------

1. Copy the views_tagger folder to sites/all/modules or to a site-specific
   modules folder.
2. Go to Administer > Site building > Modules and enable the Views Tagger
   module and the Views UI module.

The module provides a default view at /admin/content/node/tagger.


Configuration
--------------------------------------------------------------------------------

1. Go to Administer > Site building > Views and create a new view.
2. Add a couple of fields to the view, e.g. Title and Teaser.
3. Select Basic settings > Style and choose the Tagger style.
4. Create a page display and assign it a URL.
5. Go to the assigned URL and start tagging your content.

Optional: Click the gear icon next to the Tagger style and select the
vocabularies you want to see in the view. If you don't make any changes, all
vocabularies will be included in the view.


Support
--------------------------------------------------------------------------------

Please post bug reports and feature requests in the issue queue:

  http://drupal.org/project/views_tagger


Credits
--------------------------------------------------------------------------------

Author: Morten Wulff <wulff@ratatosk.net>

Based on the Views Bulk Operations module.


